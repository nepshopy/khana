<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// All installer route are here
Route::get('install','Installer\InstallerController@install')->name('install');
Route::get('install/purchase','Installer\InstallerController@purchase')->name('purchase');
Route::post('install/purchase_check','Installer\InstallerController@purchase_check')->name('purchase_check');
Route::get('install/check','Installer\InstallerController@check')->name('install.check');
Route::get('install/info','Installer\InstallerController@info')->name('install.info');
Route::get('install/migrate','Installer\InstallerController@migrate')->name('install.migrate');
Route::get('install/seed','Installer\InstallerController@seed')->name('install.seed');
Route::post('install/store','Installer\InstallerController@send');
Route::get('404',function(){
	return abort(404);
})->name(404);


// // sitemap route
Route::get('/sitemap.xml', 'SettingController@sitemapView');

Route::get('locale','LocalizationController@store')->name('language.set'); 
// laravel auth routes
Auth::routes(['verify' => true]);
Route::get('/mysettings','Admin\UserController@index')->name('admin.admin.mysettings')->middleware('auth');
Route::post('genup','Admin\UserController@genUpdate')->name('admin.users.genupdate')->middleware('auth');
Route::post('passup','Admin\UserController@updatePassword')->name('admin.users.passup')->middleware('auth');



Route::get('test', function(Request $request) {
    dd($request);
});
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| All Admin routes are here
|
|
*/

//Admin settings route
Route::group(['as' =>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth','admin']],function(){
	//dashboard route
	Route::get('dashboard','DashboardController@dashboard')->name('dashboard');
	Route::post('announcement','DashboardController@announcement')->name('announcement');
	// page resource route
	Route::resource('page','PageController');
	Route::post('pages/destroy','PageController@destroy')->name('page.destroy');

	// blog resource route
	Route::resource('blog/post','PostController');
	Route::post('post/destroy','PostController@destroy')->name('post.destroys');
	Route::resource('blog/comment','CommentController');
	Route::get('/comment/disqus','CommentController@apiview')->name('dsiqus.settings');

	Route::post('blog/comments/destroy','CommentController@destroy')->name('comments.destroy');
	Route::get('blog/comments/reply/{id}','CommentController@reply')->name('comment.reply');
	Route::resource('blog/category','CategoryController');

	Route::post('disqus','CommentController@disqus')->name('disqus.store');

	// category route
	Route::post('categorys','CategoryController@destroy')->name('categorys.destroy');
	Route::resource('settings','SettingsController');
	// menu route
	Route::resource('appearance/menu','MenuController');
	Route::post('menues/destroy','MenuController@destroy')->name('menues.destroy');
	Route::post('menues/node','MenuController@MenuNodeStore')->name('menus.MenuNodeStore');
	//theme route 
	Route::get('appearance/theme','ThemeController@index')->name('theme.index');
	Route::get('theme/{name}','ThemeController@active')->name('theme.active');
	Route::post('theme/upload','ThemeController@upload')->name('theme.upload');
	//widget route 
	Route::get('appearance/widget','WidgetController@index')->name('widget.index');
	//themeoptions route
	Route::get('appearance/themeoptions','ThemeoptionsController@index')->name('themeoptions.index');
	//script route
	Route::resource('appearance/script','ScriptController');
	//Plugin route
	Route::get('plugin','PluginController@index')->name('plugin.index');
	Route::get('plugin/active/{plugin}','PluginController@active')->name('plugin.active');
	Route::get('plugin/deactive/{plugin}','PluginController@deactive')->name('plugin.deactive');
	Route::post('plugin/upload','PluginController@upload')->name('plugin.upload');

	//seo route
	Route::resource('setting/seo','SeoController');

	//env route
	Route::resource('setting/env','EnvController');
	Route::resource('setting/filesystem','FilesystemController');

	//performance route
	Route::resource('setting/performance','PerfomaceController');


	//general route
	Route::resource('setting/general','GensettingsController');


	//permission route
	Route::get('permission','PermissionController@index')->name('permission.index');


	//usersystem route
	Route::get('usersystem','UsersystemController@index')->name('usersystem.index');


	//information route
	Route::get('information','InformationController@index')->name('information.index');

	//permissions
	Route::resource('permission','PermissionController');

	//langauge route
	Route::resource('language','LanguageController');


	//Customizer All Route
	Route::get('customizer','CustomizerController@index')->name('customizer.index');
	Route::get('customizer/page_change','CustomizerController@page_change')->name('customizer.page_change');
	Route::get('customizer/section_option','CustomizerController@section_option')->name('customizer.section_option');
	Route::get('customizer/value_update','CustomizerController@value_update')->name('customizer.value_update');
	Route::get('customizer/multiple_settings_option','CustomizerController@multiple_settings_option')->name('customizer.multiple_settings_option');
	Route::post('customizer/image_upload','CustomizerController@image_upload')->name('customizer.image_upload');
	Route::get('customizer/save','CustomizerController@save')->name('customizer.save');

});


/*
|--------------------------------------------------------------------------
| Store Routes
|--------------------------------------------------------------------------
|
| All Store routes are here
|
|
*/
Route::group(['as' =>'store.','prefix'=>'store','namespace'=>'Store','middleware'=>['auth','store','verified','approval']],function(){
	// dashboard route
	Route::get('dashboard','DashboardController@dashboard')->name('dashboard');
	Route::post('status','DashboardController@status')->name('status');
});


Route::group(['as' =>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth']],function(){
	// dashboard route
	Route::get('dashboard','DashboardController@dashboard')->name('dashboard');
	Route::resource('media','MediaController');
	Route::get('medias/json','MediaController@json')->name('medias.json');
	Route::get('media/info/{id}','MediaController@show');
	Route::post('medias/remove','MediaController@destroy')->name('medias.destroy');
	
});

// Social Login Route
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');